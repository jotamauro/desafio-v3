import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FaturasComponent } from "./historico-geral/faturas.component";
import { HistoricoPorMesComponent } from "./historico-por-mes/historico-por-mes.component";
import { HistoricoPorCategoriaComponent } from "./historico-por-categoria/historico-por-categoria.component";

const routes: Routes = [
  { path: "", component: FaturasComponent },
  { path: "geral", component: FaturasComponent },
  { path: "mes", component: HistoricoPorMesComponent },
  { path: "categoria", component: HistoricoPorCategoriaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
