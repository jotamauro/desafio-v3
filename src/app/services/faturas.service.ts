import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";

@Injectable()
export class FaturasService {
  constructor(private http: HttpClient) {}

  async getInvoce(): Promise<any> {
    return await this.http
      .get("https://desafio-it-server.herokuapp.com/lancamentos")
      .toPromise();
  }
  async getCategorias(): Promise<any> {
    return await this.http
      .get("https://desafio-it-server.herokuapp.com/categorias")
      .toPromise();
  }
}
