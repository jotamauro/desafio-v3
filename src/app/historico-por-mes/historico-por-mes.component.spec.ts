import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { HistoricoPorMesComponent } from "./historico-por-mes.component";

describe("HistoricoPorAnoComponent", () => {
  let component: HistoricoPorMesComponent;
  let fixture: ComponentFixture<HistoricoPorMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HistoricoPorMesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoPorMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
