import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoPorCategoriaComponent } from './historico-por-categoria.component';

describe('HistoricoPorCategoriaComponent', () => {
  let component: HistoricoPorCategoriaComponent;
  let fixture: ComponentFixture<HistoricoPorCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoPorCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoPorCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
