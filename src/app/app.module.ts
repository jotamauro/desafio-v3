import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import localePt from "@angular/common/locales/pt";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
registerLocaleData(localePt);

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FaturasComponent } from "./historico-geral/faturas.component";
import { FaturasService } from "./services/faturas.service";
import { HttpClientModule } from "@angular/common/http";
import { HistoricoPorMesComponent } from "./historico-por-mes/historico-por-mes.component";
import { HistoricoPorCategoriaComponent } from "./historico-por-categoria/historico-por-categoria.component";
import { registerLocaleData } from "@angular/common";
import {
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatTabsModule,
  MatDialogModule,
} from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
    FaturasComponent,
    HistoricoPorMesComponent,
    HistoricoPorCategoriaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    MatDialogModule,
  ],
  providers: [FaturasService],
  bootstrap: [AppComponent],
})
export class AppModule {}
