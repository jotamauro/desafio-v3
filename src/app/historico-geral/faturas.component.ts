import { Component, OnInit } from "@angular/core";
import { FaturasService } from "../services/faturas.service";

@Component({
  selector: "app-faturas",
  templateUrl: "./faturas.component.html",
  styleUrls: ["./faturas.component.css"],
})
export class FaturasComponent implements OnInit {
  public lancamentos = new Array<any>();
  public displayedColumns: string[] = ["geral"];
  public dataSource = [];
  constructor(private faturaService: FaturasService) {}

  ngOnInit() {
    this.getAsyncData();
  }

  async getAsyncData() {
    this.lancamentos = await this.faturaService.getInvoce();
    this.dataSource = this.lancamentos;
  }
}
